esorex (3.13.5+ds-1) unstable; urgency=medium

  * New upstream version 3.13.5+ds
  * Remove outdated greater-than versioned build dependency

 -- Ole Streicher <olebole@debian.org>  Mon, 16 Aug 2021 20:39:33 +0200

esorex (3.13.3+ds-1) unstable; urgency=medium

  * Change repack suffix to +ds
  * New upstream version 3.13.3+ds
  * Rediff patches
  * Push Standards-Version to 4.5.1. No changes needed
  * Push dh-compat to 13
  * Add "Rules-Requires-Root: no" to d/control
  * Remove duplicate pattern from d/copyright
  * Replace ADT_TMP with AUTOPKGTEST_TMP in d/tests

 -- Ole Streicher <olebole@debian.org>  Wed, 10 Feb 2021 15:59:08 +0100

esorex (3.13.2+dfsg-3) unstable; urgency=low

  * Re-enable Python tests on armhf and ppc64el (Closes: #893206)
  * Push Standards-Version to 4.5.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Mon, 03 Feb 2020 18:07:27 +0100

esorex (3.13.2+dfsg-2) unstable; urgency=medium

  * Fix testsuite failures on riscv64 (Closes: #934081)
  * Fix dversionmangle in d/watch

 -- Ole Streicher <olebole@debian.org>  Wed, 21 Aug 2019 21:12:20 +0200

esorex (3.13.2+dfsg-1) unstable; urgency=low

  * Exclude html subdir from sources
  * New upstream version 3.13.2+dfsg. Rediff patches
  * Push Standards-Version to 4.4.0. No changes needed
  * Push compat to 12
  * Add gitlab-ci.yml for salsa

 -- Ole Streicher <olebole@debian.org>  Wed, 31 Jul 2019 09:46:50 +0200

esorex (3.13.1-1) unstable; urgency=low

  * New upstream version 3.13.1. Rediff patches
  * Push Standards-Version top 4.1.4. No changes needed

 -- Ole Streicher <olebole@debian.org>  Thu, 12 Apr 2018 16:21:43 +0200

esorex (3.13-4) unstable; urgency=low

  * Disable one more json test

 -- Ole Streicher <olebole@debian.org>  Sat, 17 Mar 2018 13:13:34 +0100

esorex (3.13-3) unstable; urgency=low

  * Revert "Use avcall instead of ffi"
  * Disable failing test on armhf and ppc64el

 -- Ole Streicher <olebole@debian.org>  Sat, 17 Mar 2018 10:40:27 +0100

esorex (3.13-2) unstable; urgency=low

  * Use avcall instead of ffi to fix FTBFS on ppc64el and armhf

 -- Ole Streicher <olebole@debian.org>  Thu, 15 Mar 2018 16:37:50 +0100

esorex (3.13-1) unstable; urgency=low

  * Switch patch management to gbp
  * Update VCS fields to use salsa.d.o
  * New upstream version 3.13. Rediff patches
  * Add libffi-dev to build dependencies to enable Python support
  * Add jquery.js to missing-sources
  * Push compat to 11

 -- Ole Streicher <olebole@debian.org>  Mon, 12 Mar 2018 20:18:25 +0100

esorex (3.12.3-1) unstable; urgency=low

  * New upstream version
  * Update standard to 3.9.7. No changes needed.
  * Create minimal d/u/metadata, update VCS fields

 -- Ole Streicher <olebole@debian.org>  Fri, 19 Feb 2016 10:04:37 +0100

esorex (3.12-1) unstable; urgency=low

  * Add simple CI tests
  * switch back to unstable

 -- Ole Streicher <olebole@debian.org>  Sun, 26 Apr 2015 11:56:28 +0200

esorex (3.12-1~exp) experimental; urgency=low

  * New upstream version

 -- Ole Streicher <olebole@debian.org>  Tue, 10 Mar 2015 13:06:55 +0100

esorex (3.11-1) unstable; urgency=low

  * New upstream version
  * Build-depend on libcfitsio-dev instead of libcfitsio3-dev. Closes: #761706
  * Update uploader e-mail
  * Update standards version to 3.9.6. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Sat, 27 Sep 2014 22:33:01 +0200

esorex (3.10.2-1) unstable; urgency=low

  * New upstream version
  * Change maintainer and VCS location to debian-astro
  * Push standards version to 3.9.5. No changes needed.

 -- Ole Streicher <debian@liska.ath.cx>  Sat, 12 Apr 2014 13:16:28 +0200

esorex (3.10-1) unstable; urgency=low

  * Change distribution to unstable

 -- Ole Streicher <debian@liska.ath.cx>  Sun, 28 Apr 2013 14:35:34 +0200

esorex (3.10-1~exp2) experimental; urgency=low

  * Correct default plugin path. Closes: #704752

 -- Ole Streicher <debian@liska.ath.cx>  Mon, 08 Apr 2013 13:14:35 +0200

esorex (3.10-1~exp1) experimental; urgency=low

  * New upstream version

 -- Ole Streicher <debian@liska.ath.cx>  Mon, 18 Mar 2013 15:47:29 +0100

esorex (3.9.6-1) unstable; urgency=low

  * New upstream version
  * Add Multi-Arch default plugin directory
  * Set DM-Upload-Allowed

 -- Ole Streicher <debian@liska.ath.cx>  Fri, 11 May 2012 12:00:00 +0200

esorex (3.9.0-1) unstable; urgency=low

  * New package. Closes: #641634

 -- Ole Streicher <debian@liska.ath.cx>  Fri, 25 Nov 2011 12:08:00 +0200
